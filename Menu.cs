﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnDGameMasterTool
{
    class Menu
    {
        private List<string> mainmenu = new List<string>() 
        {
            "Edit campain",
            "Veiw campain",
        };             //main menu
        private List<string> edit_campainmenu = new List<string>()
        {
            "Edit Town",
            "Edit Npc",
            "Edit PC",
            "Edit Monster",
        };     //Edit campain menu
        private List<string> veiw_campainmenu = new List<string>()
        {
            "View Town",
            "View Npc",
            "View PC",
            "View Monster",
        };     //View campain menu




        public void Startmenu()
        {

            Menuchoice(mainmenu);
            Menuchoice(edit_campainmenu);
            Menuchoice(veiw_campainmenu);

        }

        private int Menuchoice(List<string> menuchoices)
        {
            int choice;
            bool wrongAnswer = true;
            do
            {
                for (int i = 0; i < menuchoices.Count; i++)
                {
                    Console.WriteLine("**Press {0} for {1}**", i + 1, menuchoices[i]);
                }
                Console.Write("Choice: ");
                bool idiot = int.TryParse(Console.ReadLine(), out choice);
                if (idiot && choice <= menuchoices.Count && choice > 0)
                {
                    wrongAnswer = false;
                    
                } 
            } while (wrongAnswer);
            return choice;



        }               // takes a list and turn it into menu choises and checks if input is valid befor returning it


    }
}

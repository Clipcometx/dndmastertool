﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnDGameMasterTool
{
    class NPC
    {
        private string name = "";
        private string lvl = "";

        public NPC()
        {
            name = "none";
            Lvl = "Unknown";
        }

        public NPC(string name, string lvl)
        {
            this.name = name;
            this.Lvl = lvl;
        }

        public string Name { get => name; set => name = value; }
        public string Lvl { get => lvl; set => lvl = value; }

        public void ShowInfo()
        {
            Console.WriteLine("NPC Info");
            Console.WriteLine("--------");
            Console.WriteLine("Name = {0}", name);
            Console.WriteLine("Level = {0}", Lvl);
            Console.WriteLine("");
        }

    }
}

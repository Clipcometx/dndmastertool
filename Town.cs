﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnDGameMasterTool
{
    class Town
    {
        private List<NPC> npclist = new List<NPC>();






        public void AddNpc()
        {
            NPC nPC = new NPC();
            npclist.Add(nPC);
        }
        public void AddNpc(string name, string level)
        {
            NPC nPC = new NPC(name, level);
            npclist.Add(nPC);
        }

        internal void Printnpc(string name)
        {
            for (int i = 0; i < npclist.Count; i++)
            {
                if (npclist[i].Name == name)
                {
                    npclist[i].ShowInfo();
                }

            }
        }

        public void PrintallNpc()
        {
            for (int i = 0; i < npclist.Count; i++)
            {
                npclist[i].ShowInfo();
            }
        }
    }
}
